#ifndef DNDTREEWIDGET_H
#define DNDTREEWIDGET_H

#include <QTreeWidget>

/**
 * @brief The QTreeWidget 확장 클래스
 */
class DnDTreeWidget : public QTreeWidget
{
    Q_OBJECT
public:
    explicit DnDTreeWidget(QWidget *parent = Q_NULLPTR);
    ~DnDTreeWidget();
};

#endif // DNDTREEWIDGET_H
