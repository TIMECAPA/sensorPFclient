#include "analreportdlg.h"
#include "ui_analreportdlg.h"


#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QPixmap>
#include <QDebug>
#include <QGridLayout>
#include <QDebug>
#include <time.h>       /* time */

#include <iostream>
#include <random>

/**
 * @brief 생성자
 * @param parent
 */
AnalReportDlg::AnalReportDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AnalReportDlg),
    m_bDragOver(false)
{
    ui->setupUi(this);

    ui->lcdAvgNumber->setValue(0.0000000000000);
    ui->lcdP2PNumber->setValue(0.0000000000000);
    ui->lcdRMSNumber->setValue(0.0000000000000);

    showAllFrames(false);    

    setAcceptDrops(true);

    setupCustomPlot(ui->customPlotFFT,false);
    setupCustomPlot(ui->customPlotRaw);


    ui->customPlotFFT->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->customPlotFFT, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotFFTContextMenuReq(QPoint)));

    ui->lcdAvgNumber->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->lcdAvgNumber, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotAvgLCDNumContextMenuReq(QPoint)));

    ui->lcdP2PNumber->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->lcdP2PNumber, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotP2PLCDNumContextMenuReq(QPoint)));

    ui->lcdRMSNumber->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->lcdRMSNumber, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotRMSLCDNumContextMenuReq(QPoint)));

    ui->ledSnDWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->ledSnDWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotSnDLEDContextMenuReq(QPoint)));

    ui->ledVarianceWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->ledVarianceWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotVarianceLEDContextMenuReq(QPoint)));

}

/**
 * @brief 소멸자
 */
AnalReportDlg::~AnalReportDlg()
{
    delete ui;
}

/**
 * @brief customplot 초기 설정
 * @param customPlot
 */
void AnalReportDlg::setupCustomPlot(QCustomPlot* customPlot,bool bTimeTicker)
{
#if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
  QMessageBox::critical(this, "", "You're using Qt < 4.7, the realtime data demo needs functions that are available with Qt 4.7 to work properly");
#endif

    QFont serifFont("Times", 8, QFont::Bold);

    customPlot->legend->setVisible(true);
    customPlot->legend->setFont(serifFont);

    if(bTimeTicker){
        QSharedPointer<QCPAxisTickerDateTime> timeTicker(new QCPAxisTickerDateTime);
        customPlot->xAxis->setRange(-60*3.5, 60*11);

        //timeTicker->setTimeFormat("%m:%s");
        //timeTicker->setDateTimeFormat("day dd\nhh:mm:ss");
        timeTicker->setDateTimeFormat("yy.MM.dd \nhh:mm:ss");\
        timeTicker->setDateTimeSpec(Qt::UTC);
        //timeTicker->setTickStepStrategy(QCPAxisTicker::TickStepStrategy::tssReadability);
        timeTicker->setTickCount(10);
        customPlot->xAxis->setTicker(timeTicker);
    }
    customPlot->axisRect()->setupFullAxesBox();


    // set some pens, brushes and backgrounds:
    customPlot->xAxis->setBasePen(QPen(Qt::white, 1));
    customPlot->yAxis->setBasePen(QPen(Qt::white, 1));
    customPlot->xAxis->setTickPen(QPen(Qt::white, 1));
    customPlot->yAxis->setTickPen(QPen(Qt::white, 1));
    customPlot->xAxis->setSubTickPen(QPen(Qt::white, 1));
    customPlot->yAxis->setSubTickPen(QPen(Qt::white, 1));
    customPlot->xAxis->setTickLabelColor(Qt::white);
    customPlot->yAxis->setTickLabelColor(Qt::white);
    customPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->grid()->setSubGridVisible(true);
    customPlot->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    customPlot->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    customPlot->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    customPlot->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);

    QLinearGradient plotGradient;
    plotGradient.setStart(0, 0);
    plotGradient.setFinalStop(0, 350);
    plotGradient.setColorAt(0, QColor(80, 80, 80));
    plotGradient.setColorAt(1, QColor(50, 50, 50));
    customPlot->setBackground(plotGradient);

    QLinearGradient axisRectGradient;
    axisRectGradient.setStart(0, 0);
    axisRectGradient.setFinalStop(0, 350);
    axisRectGradient.setColorAt(0, QColor(80, 80, 80));
    axisRectGradient.setColorAt(1, QColor(30, 30, 30));
    customPlot->axisRect()->setBackground(axisRectGradient);

    customPlot->setInteraction(QCP::iRangeDrag);
    //ui->customPlot->axisRect()->setRangeDrag(Qt::Vertical|Qt::Horizontal);
    customPlot->axisRect()->setRangeDragAxes(customPlot->xAxis,customPlot->yAxis);

    customPlot->setInteraction(QCP::iRangeZoom);
    customPlot->axisRect()->setRangeZoom(Qt::Vertical|Qt::Horizontal);
    // make left and bottom axes transfer their ranges to right and top axes:
    //  connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->xAxis2, SLOT(setRange(QCPRange)));
    //  connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->yAxis2, SLOT(setRange(QCPRange)));
    customPlot->setInteraction(QCP::iSelectItems);

}

/**
 * @brief 드래그 Enter 이벤트 처리
 * @param event
 */
void AnalReportDlg::dragEnterEvent(QDragEnterEvent *event)
{

    if (event->mimeData()->hasImage()) {
        event->setAccepted(true);
        m_bDragOver = true;
        update();
    } else {
        AnalReportDlg::dragEnterEvent(event);
    }
}

/**
 * @brief 드래그 Move 이벤트 처리
 * @param event
 */
void AnalReportDlg::dragMoveEvent(QDragMoveEvent *event)
{
    //QRect rect = m_pMainGridLayout->cellRect(1,1);
    if(event->mimeData()->hasText()){
        QFrame* pFrame = currentDragFrame(event->mimeData()->text());
        if(pFrame){
            QRect rectPlot = pFrame->rect();
            QPoint pos = event->pos();
            qDebug() << "dragMoveEvent Pos x : " << pos.x() << "Pos y :" << pos.y() << endl;

            m_rectHighlighted.setLeft(pos.x());
            m_rectHighlighted.setTop(pos.y());
            m_rectHighlighted.setRight(pos.x()+rectPlot.width());
            m_rectHighlighted.setBottom(pos.y()+rectPlot.height());

            event->setDropAction(Qt::MoveAction);
            event->accept();
            //QRect updateRect = highlightedRect.united(targetSquare(event->pos()));
            update();
        }
    }

}

/**
 * @brief 드래그 Leave 이벤트 처리
 * @param event
 */
void AnalReportDlg::dragLeaveEvent(QDragLeaveEvent *event)
{
    Q_UNUSED(event);
    m_bDragOver = false;

//  QRect updateRect = highlightedRect;
    m_rectHighlighted = QRect();
//  update(updateRect);
    update();
}

/**
 * @brief 드래그 이벤트 처리
 * @param event
 */
void AnalReportDlg::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasImage()) {
        m_bDragOver = false;
        //pixmap = qvariant_cast<QPixmap>(event->mimeData()->imageData());
        if(event->mimeData()->hasText()){
            qDebug() << "dropEvent MimeData Text : " << event->mimeData()->text() << endl;
            QPoint pos = event->pos();
            QFrame* pFrame = currentDragFrame(event->mimeData()->text());
            if(pFrame){
                pFrame->setGeometry(pos.x(),pos.y(),pFrame->rect().width(),pFrame->rect().height());
                pFrame->show();
            }
        }
        m_rectHighlighted = QRect();
        update();
    } else {
        QDialog::dropEvent(event);
    }
}

/**
 * @brief 그리기 이벤트
 * @param event
 */
void AnalReportDlg::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.fillRect(event->rect(), Qt::white);

    if (m_rectHighlighted.isValid()) {
        painter.setBrush(QColor("#ffcccc"));
        painter.setPen(Qt::NoPen);
        painter.drawRect(m_rectHighlighted.adjusted(0, 0, -1, -1));
    }

    //    for (int i = 0; i < pieceRects.size(); ++i)
    //        painter.drawPixmap(pieceRects[i], piecePixmaps[i]);
    painter.end();
}

/**
 * @brief 마우스 Press 이벤트
 * @param event
 */
void AnalReportDlg::mousePressEvent(QMouseEvent *event)
{

    QDialog::mousePressEvent(event);
}

/**
 * @brief 마우스 Release 이벤트
 * @param event
 */
void AnalReportDlg::mouseReleaseEvent(QMouseEvent *event)
{

    QDialog::mouseReleaseEvent(event);
}

/**
 * @brief 마우스 DoubleClick 이벤트
 * @param event
 */
void AnalReportDlg::mouseDoubleClickEvent(QMouseEvent *event)
{
    QDialog::mouseDoubleClickEvent(event);
}

/**
 * @brief 마우스 Move 이벤트
 * @param event
 */
void AnalReportDlg::mouseMoveEvent(QMouseEvent *event)
{    
    update(); //Frame에 afterimage 문제 때문
    QDialog::mouseMoveEvent(event);
}

/**
 * @brief 현재 드래그 Frame 지정
 * @param strToolBarName
 * @return
 */
QFrame* AnalReportDlg::currentDragFrame(QString strToolBarName)
{
    QFrame* pFrame = NULL;
    if(strToolBarName.compare(DEF_OF_FFTPLOT)==0){
        pFrame = ui->customPlotFFTFrame;
    }else if(strToolBarName.compare(DEF_OF_RAWPLOT)==0){
        pFrame = ui->customPlotRawFrame;
    }else if(strToolBarName.compare(DEF_OF_RMSLCDNUM)==0){
        pFrame = ui->numRMSLCDFrame;
    }else if(strToolBarName.compare(DEF_OF_P2PLCDNUM)==0){
        pFrame = ui->numP2PLCDFrame;
    }else if(strToolBarName.compare(DEF_OF_AVGLCDNUM)==0){
        pFrame = ui->numAvgLCDFrame;
    }else if(strToolBarName.compare(DEF_OF_VARIANCELED)==0){
        pFrame = ui->ledVarianceFrame;
    }else if(strToolBarName.compare(DEF_OF_STDEVLED)==0){
        pFrame = ui->ledSnDFrame;
    }
    return pFrame;
}

/**
 * @brief 분석 툴 RePlot
 * @param nIdx
 * @param strGraphName
 * @param x
 * @param y
 */
void AnalReportDlg::replotAnalTool(int nIdx,QString strGraphName,QVector<double>& x, QVector<double>& y)
{
    QCustomPlot* customPlot=NULL;
    if(nIdx==0){
        customPlot = ui->customPlotRaw;
    }else{
        customPlot = ui->customPlotFFT;
    }

    if(customPlot->graphCount()==0){
        customPlot->addGraph();
        customPlot->graph(0)->setName(strGraphName);        
        customPlot->graph(0)->setPen(QPen(QColor(255, 0, 0), 2));
        customPlot->graph(0)->setData(x, y);
        customPlot->graph(0)->rescaleAxes();
    }else if(customPlot->graphCount()==1){
        customPlot->addGraph();
        customPlot->graph(1)->setName(strGraphName);
        customPlot->graph(1)->setPen(QPen(QColor(0, 255, 0), 2));
        customPlot->graph(1)->setData(x, y);
        customPlot->graph(1)->rescaleAxes();
    }
    customPlot->xAxis->setLabel("시간");
    customPlot->yAxis->setLabel("값");
    customPlot->replot();
}



/**
 * @brief 평균 값 설정
 * @param dbAvg
 */
void AnalReportDlg::setAvg(double dbAvg)
{
//    mt19937::result_type seed = time(0);
//    auto dice_rand = std::bind(std::uniform_int_distribution<int>(1,6),mt19937(seed));

    /* initialize random seed: */
    srand (time(NULL));
    /* generate secret number between 1 and 10: */
    int nIndex = rand() % 5;
    //ui->lcdAvgNumber->updateLCDNumerColor();
    ui->lcdAvgNumber->setValue(dbAvg);
}

/**
 * @brief P2P  설정
 * @param dbP2P
 */
void AnalReportDlg::setP2P(double dbP2P)
{
    //ui->lcdP2PNumber->updateLCDNumerColor();
    ui->lcdP2PNumber->setValue(dbP2P);
}

/**
 * @brief RMS  설정
 * @param dbRMS
 */
void AnalReportDlg::setRMS(double dbRMS)
{
    //ui->lcdRMSNumber->updateLCDNumerColor();
    ui->lcdRMSNumber->setValue(dbRMS);
}

/**
 * @brief 분산  설정
 * @param dbVal
 */
void AnalReportDlg::setVariance(double dbVal)
{    
    ui->ledVarianceWidget->setValue(dbVal);
}

/**
 * @brief 표준 편차 설정
 * @param dbVal
 */
void AnalReportDlg::setSnD(double dbVal)
{
    ui->ledSnDWidget->setValue(dbVal);
}

/**
 * @brief 컨트롤 초기화
 */
void AnalReportDlg::clearAll()
{
    clearPlot();
    ui->lcdAvgNumber->setValue(0.0);
    ui->lcdP2PNumber->setValue(0.0);
    ui->lcdRMSNumber->setValue(0.0);
    ui->ledVarianceWidget->setValue(0.0);
    ui->ledSnDWidget->setValue(0.0);
}

/**
 * @brief customplot 클리어
 * @param nIdx
 */
void AnalReportDlg::clearPlot(int nIdx)
{
    Q_UNUSED(nIdx)

    ui->customPlotRaw->clearGraphs();
    ui->customPlotRaw->replot();

    ui->customPlotFFT->clearGraphs();
    ui->customPlotFFT->replot();

}

/**
 * @brief 모든 Frame 보기/숨기기
 * @param bShow
 */
void AnalReportDlg::showAllFrames(bool bShow)
{
    ui->customPlotRawFrame->setVisible(bShow);
    ui->customPlotFFTFrame->setVisible(bShow);
    ui->numRMSLCDFrame->setVisible(bShow);
    ui->numP2PLCDFrame->setVisible(bShow);
    ui->numAvgLCDFrame->setVisible(bShow);
    ui->ledVarianceFrame->setVisible(bShow);
    ui->ledSnDFrame->setVisible(bShow);
}

/**
 * @brief UI 디폴트 처리
 * @param uiAppliedAlgorithm
 */
void AnalReportDlg::setDefaultUI(quint32 uiAppliedAlgorithm)
{
    if((uiAppliedAlgorithm & 0x1)==0){//FFT
        ui->customPlotFFT->clearGraphs();
        ui->customPlotFFT->replot();
    }
    if((uiAppliedAlgorithm & 0x2)==0){//RMS
        ui->lcdRMSNumber->setValue(0.0);
    }
    if((uiAppliedAlgorithm & 0x4)==0){//P2P
        ui->lcdP2PNumber->setValue(0.0);
    }
    if((uiAppliedAlgorithm & 0x8)==0){//Average
        ui->lcdAvgNumber->setValue(0.0);
    }
    if((uiAppliedAlgorithm & 0x10)==0){//Variance
        ui->ledVarianceWidget->setValue(0.0);
    }
    if((uiAppliedAlgorithm & 0x11)==0){//SnD
        ui->ledSnDWidget->setValue(0.0);
    }
}

/**
 * @brief FFT 컨텍스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotFFTContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowFFTSetupDock()));
    menu->popup(ui->customPlotFFT->mapToGlobal(pos));
}

/**
 * @brief Avg LCD 컨택스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotAvgLCDNumContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowAvgLCDNumSetupDock()));
    menu->popup(ui->lcdAvgNumber->mapToGlobal(pos));
}

/**
 * @brief P2P LCD 컨텍스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotP2PLCDNumContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowP2PLCDNumSetupDock()));
    menu->popup(ui->lcdP2PNumber->mapToGlobal(pos));
}

/**
 * @brief RMS LCD 컨텍스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotRMSLCDNumContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowRMSLCDNumSetupDock()));
    menu->popup(ui->lcdRMSNumber->mapToGlobal(pos));
}

/**
 * @brief 분산 LED 컨텍스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotVarianceLEDContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowVarianceLEDSetupDock()));
    menu->popup(ui->ledVarianceWidget->mapToGlobal(pos));
}

/**
 * @brief 표준 편차 컨텍스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotSnDLEDContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowSnDLEDSetupDock()));
    menu->popup(ui->ledSnDWidget->mapToGlobal(pos));
}

/**
 * @brief FFT 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowFFTSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showFFTSetupDock(true);

}

/**
 * @brief Avg LCD 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowAvgLCDNumSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showLCDNumSetupDock(0,true);
}

/**
 * @brief P2P LCD 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowP2PLCDNumSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showLCDNumSetupDock(1,true);
}

/**
 * @brief RMS LCD 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowRMSLCDNumSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showLCDNumSetupDock(2,true);
}

/**
 * @brief 분산 LED 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowVarianceLEDSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showLEDSetupDock(0,true);
}

/**
 * @brief 표준 편차 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowSnDLEDSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showLEDSetupDock(1,true);
}

/**
 * @brief FFT 데이터 파일로 저장
 */
void AnalReportDlg::saveFFTDataToFile()
{
    QString filters("Text files (*.txt);;All files (*.*)");
    QString defaultFilter("Text files (*.txt)");
    QTextCodec *textCodec = QTextCodec::codecForName("eucKR");
    QString strTitle = "저장하기";
    strTitle = textCodec->toUnicode(strTitle.toLocal8Bit());

    QString strSaveFileName = QFileDialog::getSaveFileName(0, strTitle, QDir::currentPath(), filters, &defaultFilter);
    QFile file(strSaveFileName);
    if(file.open(QIODevice::WriteOnly)){
        if(ui->customPlotFFT->graph(0)){
            QSharedPointer<QCPGraphDataContainer> data =ui->customPlotFFT->graph(0)->data();
            if(data){
                QString strKeyVal="0.0";
                QString strVal="0.0";
                QTextStream out(&file);
                QVector<QCPGraphData>::iterator it = data->begin();
                const QVector<QCPGraphData>::iterator itEnd = data->end();
                int i = 0;
                QDateTime dateTime;
                qint64 qiMSec = 0;
                while (it != itEnd) {
                    qiMSec = (it->key)*1000;
                    dateTime = QDateTime::fromMSecsSinceEpoch(qiMSec);
                    dateTime = dateTime.addSecs(-9*60*60);
                    //strKeyVal.sprintf("%.3f %.18f",it->key,it->value);
                    strKeyVal.sprintf("%s %.18f",dateTime.toString("yyyyMMdd_HHmmss.zzz").toLocal8Bit().constData(),it->value);
                    out << strKeyVal << "\r\n";
                    ++it;
                    ++i;
                }
            }
        }
    }
}

/**
 * @brief 컨트롤 속성 아이템 설정
 * @param options
 */
void AnalReportDlg::setControlOptions(ControlOptions& options)
{
    MapControlOtpions::iterator it = m_mapControlOptions.find(options.id());
    if(it == m_mapControlOptions.end()){
        m_mapControlOptions.insert(PairControlOptions(options.id(),options));
    }
    updateControl(options);
}

/**
 * @brief 컨트롤 업데이트
 * @param options
 */
void AnalReportDlg::updateControl(ControlOptions& options)
{
    QString strID = options.id();
    if(strID.compare(DEF_OF_FFTPLOT)==0){
        ui->customPlotFFT->replot();
    }else if(strID.compare(DEF_OF_RAWPLOT)==0){
        ui->customPlotRaw->replot();
    }else if(strID.compare(DEF_OF_RMSLCDNUM)==0){
        ui->lcdRMSNumber->setUseUpperLimit(options.useUpperLimit());
        ui->lcdRMSNumber->setUpperLimit(options.upperLimit());
        ui->lcdRMSNumber->setUseLowerLimit(options.useLowerLimit());
        ui->lcdRMSNumber->setLowerLimit(options.lowerLimit());
        ui->lcdRMSNumber->updateLCDNumerColor();
    }else if(strID.compare(DEF_OF_P2PLCDNUM)==0){
        ui->lcdP2PNumber->setUseUpperLimit(options.useUpperLimit());
        ui->lcdP2PNumber->setUpperLimit(options.upperLimit());
        ui->lcdP2PNumber->setUseLowerLimit(options.useLowerLimit());
        ui->lcdP2PNumber->setLowerLimit(options.lowerLimit());
        ui->lcdP2PNumber->updateLCDNumerColor();
    }else if(strID.compare(DEF_OF_AVGLCDNUM)==0){
        ui->lcdAvgNumber->setUseUpperLimit(options.useUpperLimit());
        ui->lcdAvgNumber->setUpperLimit(options.upperLimit());
        ui->lcdAvgNumber->setUseLowerLimit(options.useLowerLimit());
        ui->lcdAvgNumber->setLowerLimit(options.lowerLimit());
        ui->lcdAvgNumber->updateLCDNumerColor();
    }else if(strID.compare(DEF_OF_VARIANCELED)==0){
        ui->ledVarianceWidget->setUseUpperLimit(options.useUpperLimit());
        ui->ledVarianceWidget->setUpperLimit(options.upperLimit());
        ui->ledVarianceWidget->setUseLowerLimit(options.useLowerLimit());
        ui->ledVarianceWidget->setLowerLimit(options.lowerLimit());
        ui->ledVarianceWidget->updateLEDColor();
    }else if(strID.compare(DEF_OF_STDEVLED)==0){
        ui->ledSnDWidget->setUseUpperLimit(options.useUpperLimit());
        ui->ledSnDWidget->setUpperLimit(options.upperLimit());
        ui->ledSnDWidget->setUseLowerLimit(options.useLowerLimit());
        ui->ledSnDWidget->setLowerLimit(options.lowerLimit());
        ui->ledSnDWidget->updateLEDColor();
    }else{
    }

}
