#ifndef ANALREPORTDLG_H
#define ANALREPORTDLG_H

#include <QDialog>
#include "qcustomplot.h"
#include "mainwindow.h"

class QFrame;
class QCustomPlot;
class QGridLayout;

class ControlOptions;



namespace Ui {
class AnalReportDlg;
}

/**
 * @brief 분석보고서 다이얼로그 클래스
 */
class AnalReportDlg : public QDialog
{
    Q_OBJECT
public:
    explicit AnalReportDlg(QWidget *parent = 0);
    ~AnalReportDlg();
private:
    Ui::AnalReportDlg *ui;    
    typedef std::map<QString,ControlOptions&> MapControlOtpions;   
    typedef std::pair<QString,ControlOptions&> PairControlOptions;
    /**
     * @brief 컨트롤 속성 아이템 관리 맵
     */
    MapControlOtpions m_mapControlOptions;
private slots:
    void slotFFTContextMenuReq(QPoint pos);
    void slotAvgLCDNumContextMenuReq(QPoint pos);
    void slotP2PLCDNumContextMenuReq(QPoint pos);
    void slotRMSLCDNumContextMenuReq(QPoint pos);
    void slotVarianceLEDContextMenuReq(QPoint pos);
    void slotSnDLEDContextMenuReq(QPoint pos);

    void slotShowFFTSetupDock();
    void slotShowAvgLCDNumSetupDock();
    void slotShowP2PLCDNumSetupDock();
    void slotShowRMSLCDNumSetupDock();
    void slotShowVarianceLEDSetupDock();
    void slotShowSnDLEDSetupDock();

private:
    /**
     * @brief 드래그 Over 상태
     */
    bool  m_bDragOver;
    /**
     * @brief 드래그 시 하이라이트 Rect
     */
    QRect m_rectHighlighted;

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
    void dragEnterEvent(QDragEnterEvent *event) Q_DECL_OVERRIDE;
    void dragMoveEvent(QDragMoveEvent *event) Q_DECL_OVERRIDE;
    void dragLeaveEvent(QDragLeaveEvent *event) Q_DECL_OVERRIDE;
    void dropEvent(QDropEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseDoubleClickEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
private:    
    void setupCustomPlot(QCustomPlot* customPlot,bool bTimeTicker=true);
    QFrame* currentDragFrame(QString strToolBarName);
    void showAllFrames(bool bShow);
    void updateControl(ControlOptions& options);
 public:
    void replotAnalTool(int nIdx,QString strGraphName,QVector<double>& x, QVector<double>& y);
    void clearAll();
    void clearPlot(int nIdx=-1);
    void setDefaultUI(quint32 uiAppliedAlgorithm);
    void setAvg(double dbAvg);
    void setP2P(double dbP2P);
    void setRMS(double dbRMS);
    void setVariance(double dbVal);
    void setSnD(double dbVal);
    void saveFFTDataToFile();
    void setControlOptions(ControlOptions& options);
};

#endif // ANALREPORTDLG_H
