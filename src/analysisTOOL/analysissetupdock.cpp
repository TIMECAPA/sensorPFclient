#include "analysissetupdock.h"
#include "ui_analysissetupdock.h"

#include <QDebug>
#include <QStringList>
#include <QtSql>
#include <QMessageBox>
#include "mainwindow.h"

/**
 * @brief 생성자
 * @param parent
 */
AnalysisSetupDock::AnalysisSetupDock(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::AnalysisSetupDock)
{
    ui->setupUi(this);
    ui->cboAlgorithm->addItem("FFT");
    ui->cboAlgorithm->addItem("RMS");
    ui->cboAlgorithm->addItem("P2P");
    ui->cboAlgorithm->addItem("Average");
    ui->cboAlgorithm->addItem("Variance");//분산
    ui->cboAlgorithm->addItem("S/D"); //Standard Deviation
}

/**
 * @brief 소멸자
 */
AnalysisSetupDock::~AnalysisSetupDock()
{
    delete ui;
}

/**
 * @brief 분석 대상 센서 추가 클릭 이벤트
 */
void AnalysisSetupDock::on_btnAdd_clicked()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    QString strContainerName = ui->cboContainer->currentText();
    QString strAlgorithm     = ui->cboAlgorithm->currentText();
    if(!strContainerName.isEmpty() || !strAlgorithm.isEmpty() ){
       main->updateSensorListDock(strContainerName,strAlgorithm);
    }
}

/**
 * @brief 컨테이너 정보 가져오기
 * @param dbMySQL
 * @param strQuery
 */
void AnalysisSetupDock::addData(QSqlDatabase& dbMySQL, QString strQuery)
{
    ui->cboContainer->clear();
    if (!dbMySQL.open()){
        QMessageBox::critical(0, QObject::tr("Database Error"),
        dbMySQL.lastError().text());
    }
    // QSqlQuery query("SELECT * FROM cnt where ri like '%smartcs%'");
    QSqlQuery sqlQuery;
    sqlQuery.exec(strQuery);

    QString strContainerName;
    QStringList strlistContainers;

    int index=0;
    while(sqlQuery.next())
    {
        strContainerName = sqlQuery.value(0).toString();
        strlistContainers = strContainerName.split("/");

        if(strlistContainers.size() == 4){ // 총 4개로 구분된 컨테이너만 정상적인 컨테이너로 저장함
//            QString strContainerName = "/" + strlistContainers.at(0) + "/" + strlistContainers.at(1) +
//                    "/" + strlistContainers.at(2) + "/" + strlistContainers.at(3);
            ui->cboContainer->addItem(strContainerName);
            index++;
        }
    }
    dbMySQL.close();
}
