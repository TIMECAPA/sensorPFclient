#ifndef SENSORLISTDOCK_H
#define SENSORLISTDOCK_H

#include <QDockWidget>

class QTreeWidgetItem;
class QSqlDatabase;

namespace Ui {
class SensorListDock;
}

/**
 * @brief 센서 리스트 도킹 창 클래스
 */
class SensorListDock : public QDockWidget
{
    Q_OBJECT
public:
    explicit SensorListDock(QWidget *parent = 0);
    ~SensorListDock();
private:
    Ui::SensorListDock *ui;
public:
    void treeWidgetClear();       
signals:
    void signalUpdatePlot(QTreeWidgetItem *item, int column);
private slots:    
    void slotShowContextMenu(QPoint pos);
    void slotSetConnection(QString strSelInfo);
private:
    /**
     * @brief 컨텍스 메뉴 시점 TreeWidgetItem
     */
    QTreeWidgetItem*    m_ptreeItemOnContext;

    /**
     * @brief 현재 센서 이름
     */
    QString             m_strCurrSensorName;

    /**
     * @brief 현재 센선 알고리즘
     */
    QVector<QString>    m_vCurrSensorAlgorithms;
private:
    //void matchPlot(int nPlotIdx=0);
    int  treeWidgetDepth(QTreeWidgetItem* item);
    void setCurrentSensorAlgorithms(QTreeWidgetItem* item);
public:
    void updateSensorList(QString strContainser,QString strAlgorithm);
    QTreeWidgetItem* addChildToListWidget(QString strContents, QTreeWidgetItem* itemParent, int nColumn);
    QVector<QString>* currentSensorAlgorithms();

};

#endif // SENSORLISTDOCK_H
