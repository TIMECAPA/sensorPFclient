#include "lcdnumsetupdock.h"
#include "ui_lcdnumsetupdock.h"

/**
 * @brief 생성자
 * @param parent
 */
LCDNumSetupDock::LCDNumSetupDock(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::LCDNumSetupDock)
{
    ui->setupUi(this);
    ui->frameRed->setStyleSheet("background-color: red");
    ui->frameGreen->setStyleSheet("background-color: green");
    ui->frameYellow->setStyleSheet("background-color: yellow");
    ui->dbspinUpperLimit->setValue(1.000000);
    ui->dbspinLowerLimit->setValue(-1.000000);

    m_strID = "";
    bool bChecked = ui->chkUseUpperLimit->checkState() == Qt::Checked;
    m_controlOptions.setUseUpperLimit(bChecked);
    m_controlOptions.setUpperLimit(ui->dbspinUpperLimit->value());
    bChecked = ui->chkUseLowerLimit->checkState() == Qt::Checked;
    m_controlOptions.setUseLowerLimit(bChecked);
    m_controlOptions.setLowerLimit(ui->dbspinLowerLimit->value());

    m_pMainWindow = qobject_cast<MainWindow*>(parent);
}

/**
 * @brief 소멸자
 */
LCDNumSetupDock::~LCDNumSetupDock()
{
    delete ui;
}

/**
 * @brief ID 설정
 * @param strID
 */
void LCDNumSetupDock::setID(QString strID)
{
    m_strID = strID;
    m_controlOptions.setID(strID);
}

/**
 * @brief ID 가져오기
 * @return
 */
QString LCDNumSetupDock::id() const
{
    return m_strID;
}

/**
 * @brief 상한 값 사용 체크 이벤트
 */
void LCDNumSetupDock::on_chkUseUpperLimit_clicked()
{
    m_controlOptions.setUseUpperLimit(ui->chkUseUpperLimit->checkState()==Qt::Checked);
    if(m_pMainWindow) m_pMainWindow->setControlOptions(m_controlOptions);

}

/**
 * @brief 상한 값 스핀 값 변경 이벤트
 * @param arg1
 */
void LCDNumSetupDock::on_dbspinUpperLimit_valueChanged(double arg1)
{
    m_controlOptions.setUpperLimit(arg1);
    bool bChecked = ui->chkUseUpperLimit->checkState()==Qt::Checked;
    if(bChecked) m_pMainWindow->setControlOptions(m_controlOptions);
}

/**
 * @brief 하한 값 사용 체크 이벤트
 */
void LCDNumSetupDock::on_chkUseLowerLimit_clicked()
{
    m_controlOptions.setUseLowerLimit(ui->chkUseLowerLimit->checkState() == Qt::Checked);
    if(m_pMainWindow) m_pMainWindow->setControlOptions(m_controlOptions);
}

/**
 * @brief 하한 값 스핀 값 변경 이벤트
 * @param arg1
 */
void LCDNumSetupDock::on_dbspinLowerLimit_valueChanged(double arg1)
{
    bool bChecked = ui->chkUseLowerLimit->checkState()==Qt::Checked;
    m_controlOptions.setLowerLimit(arg1);
    if(bChecked) m_pMainWindow->setControlOptions(m_controlOptions);
}
