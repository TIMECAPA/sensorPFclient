#ifndef MAINREALTIMEPLOTDOCK_H
#define MAINREALTIMEPLOTDOCK_H

#include <QDockWidget>
#include <QTimer>
#include "qcustomplot.h"
namespace Ui {
class MainRealtimePlotDock;
}

/**
 * @brief 실시간 Plot 도킹 창 클래스
 */
class MainRealtimePlotDock : public QDockWidget
{
    Q_OBJECT
public:
    explicit MainRealtimePlotDock(QWidget *parent = 0);
    ~MainRealtimePlotDock();
private:
    Ui::MainRealtimePlotDock *ui;
private:
    /**
     * @brief 실시간 타이머
     */
    QTimer dataTimer;

    /**
     * @brief 최대 값
     */
    double         m_dbYMax;

    /**
     * @brief 최소 값
     */
    double         m_dbYMin;

public slots:
    void slotUpdateMainRealtimePlot();
    void slotPlotItemClick(QCPAbstractItem *item, QMouseEvent *event);

private:
    void setupMainRealtimePlot(QCustomPlot *customPlot);
    void realRenderingChart(QString strName, double dblValue);

public:
    void addGraphToPlot(QString strName="");
    void removeGraphToPlot(QString strRemoveGraphName);
    void processMQTTMsg(QString strTopicName,QString strData,QString strTopicType);
};

#endif // MAINREALTIMEPLOTDOCK_H
