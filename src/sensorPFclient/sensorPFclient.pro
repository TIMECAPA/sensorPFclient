TEMPLATE = app

QT += network
QT += widgets
QT += gui
QT += sql
QT += xml

qtHaveModule(printsupport): QT += printsupport

HEADERS         = mainwindow.h \
                  qcustomplot.h \
    dialog.h \
    displaydbsensor.h \
    controlsensordlg.h \
    dbdataanaltooldlg.h \
    logindlg.h \
    playmqtt.h \
    realtimeplotdock.h \
    mainrealtimeplotdock.h
SOURCES         = main.cpp \
                  mainwindow.cpp \
                  qcustomplot.cpp \
    dialog.cpp \
    displaydbsensor.cpp \
    controlsensordlg.cpp \
    dbdataanaltooldlg.cpp \
    logindlg.cpp \
    playmqtt.cpp \
    realtimeplotdock.cpp \
    mainrealtimeplotdock.cpp
RESOURCES       = sensorPFclient.qrc

FORMS += \
    dialog.ui \
    displaydbsensor.ui \
    controlsensordlg.ui \
    dbdataanaltooldlg.ui \
    logindlg.ui \
    playmqtt.ui \
    realtimeplotdock.ui \
    mainrealtimeplotdock.ui




win32: LIBS += -L$$PWD/../../sdk/mysql-connector-c-6.1.6-win32/lib/ -llibmysql

INCLUDEPATH += $$PWD/../../sdk/mysql-connector-c-6.1.6-win32/include
DEPENDPATH += $$PWD/../../sdk/mysql-connector-c-6.1.6-win32/include



macx: LIBS += -L$$PWD/../../sdk/mysql-connector-c-6.1.6-osx10.8-x86_64/lib/ -lmysqlclient

INCLUDEPATH += $$PWD/../../sdk/mysql-connector-c-6.1.6-osx10.8-x86_64/include
DEPENDPATH += $$PWD/../../sdk/mysql-connector-c-6.1.6-osx10.8-x86_64/include

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../sdk/qmqtt/lib/ -lqmqtt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../sdk/qmqtt/lib/ -lqmqttd
else:macx: LIBS += -L$$PWD/../../sdk/qmqtt/lib/ -lqmqtt

INCLUDEPATH += $$PWD/../../sdk/qmqtt/inc
DEPENDPATH += $$PWD/../../sdk/qmqtt/inc

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../sdk/RayUtils/lib/ -lRayUtils
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../sdk/RayUtils/lib/ -lRayUtils

INCLUDEPATH += $$PWD/../../sdk/RayUtils/inc
DEPENDPATH += $$PWD/../../sdk/RayUtils/inc
