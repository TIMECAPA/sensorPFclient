/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>

#include <QTcpSocket>
#include <QAbstractSocket>
#include <qmqtt.h>


#include "mainrealtimeplotdock.h"
#include "realtimeplotdock.h"


QT_BEGIN_NAMESPACE
class QAction;
class QListWidget;
class QTreeWidget;
class QMenu;
class QTextEdit;
class QImage;
class QLabel;
class DispSensor;
class DisplayDBsensor;
class QTreeWidgetItem;

QT_END_NAMESPACE

class QCustomPlot;
class Dialog;
class ControlSensorDlg;


/**
 * @brief 메인 윈도우 클래스
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT   
public:
    MainWindow();
    ~MainWindow();    
private slots:
    void save();
    void print();
    void undo();
    void about();
public slots:
    // socket
    void connected();
    void disconnected();
    void readyRead();
    void slotUpdatePlot(QTreeWidgetItem *item, int column);
    // mqtt
    void MqttConnSlot();
    void MqttDisconnSlot();
    void MqttReceivedSlot(const QMQTT::Message message);
    void MqttErrorSlot();
private:
    void showAnalToolDlg();
    void showLoginDlg();
    void showPlayMqttWidget();
    void createActions();
    void createStatusBar();
    void createDockWindows();
    void setDockOptions();

private:
    /**
     * @brief 프로그램 버전
     */
    QString m_strVerName;//"sensorPFclient-v1123c";

    /**
     * @brief MQTT 클라이언트
     */
    QMQTT::Client *m_pMqttClient;

    /**
     * @brief 미 구현
     */
    QTextEdit *textEdit;

    /**
     * @brief View 메뉴
     */
    QMenu *viewMenu;

    /**
     * @brief 메인 윈도우 메뉴
     */
    QMenu *mainWindowMenu;

    /**
     * @brief 소켓
     */
    QTcpSocket *socket;


    /**
     * @brief 패킷 전송 및 데이터베이스 연동
     */
    Dialog              *m_pDialog;

    /**
     * @brief 센서 상태 및 제어
     */
    ControlSensorDlg    *m_pControlDlg;

    /**
     * @brief 실시간 Plot  도킹 창
     */
    MainRealtimePlotDock*    m_pMainRealtimePlotDock;


    typedef QMap<QString ,QDockWidget *> RealTimePlotMap;

    /**
     * @brief 실시간 Plot 맵
     */
    RealTimePlotMap realtimePlotMap;
public:
    DisplayDBsensor *dispDBsensor; // loginDlg cal
private:
    void createRealtimeDock(QString strRI);
    void destroyRealtimeDock(QString strRI);
public:

    void sockConn();
    void TestDialog();
    void ControlSensor();
    QMQTT::Client* getMQTTInstance();
    DisplayDBsensor*    getDispSensorInstance();
    //enum EN_PLOT_NAME { PLOT1=0,PLOT2,PLOT3};     
public:
    void manageRealtimeDock(const int nIdx, QString strSensorName);
    QDockWidget* realtimePlotDock(QString strDockName);
    void manageGraphToPlot(QString strRI, QString strGraphName, Qt::CheckState checkState);
};

#endif
