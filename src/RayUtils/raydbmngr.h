#ifndef RAYDBMNGR_H
#define RAYDBMNGR_H

#include <QtSql>

/**
 * @brief 데이터베이스 메니저 클래스
 */
class RayDBMngr
{
public:
    RayDBMngr();
private:
    /**
     * @brief 데이터베이스 인스턴스
     */
    QSqlDatabase m_db;
public:
    void init();
    bool open();    
    void close();

    QSqlDatabase* getDatabase();
};

#endif // RAYDBMNGR_H
