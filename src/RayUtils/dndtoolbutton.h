#ifndef DNDTOOLBUTTON_H
#define DNDTOOLBUTTON_H

#include <QToolButton>

/**
 * @brief QToolButton 확장 class
 */
class DnDToolButton : public QToolButton
{
    Q_OBJECT
public:    
    explicit DnDToolButton(const QString &title, QWidget *parent = Q_NULLPTR);
    ~DnDToolButton();
private:
    /**
     * @brief 마우스 Press 상태
     */
    bool m_bPressed;

    /**
     * @brief 제목
     */
    QString m_strTitle;
protected:
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
};

#endif // DNDTOOLBUTTON_H
