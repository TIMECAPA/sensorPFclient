#ifndef RAYMATH_H
#define RAYMATH_H

#include <complex>
#include <iostream>
#include <valarray>
#include <vector>
#include <cmath>
#include <numeric>

const double PI = 3.141592653589793238460;

typedef std::complex<double> Complex;
typedef std::valarray<Complex> CArray;

/**
 * @brief 수학 알고리즘 유틸 클래스
 */
class RayMath
{
public:
    RayMath();
public:
    //RMS - ROOT MEAN SQUARE
    static double avg(const std::vector<double>& vNumbers); //평균
    static double p2p(const std::vector<double>& vNumbers);
    static double rms(const std::vector<double>& vNumbers);

    static double variance(const std::vector<double>& vNumbers);//분산
    static double stdev(const std::vector<double>& vNumbers);//표준편차

    //FFT - FAST FOURIER TRANSFORM
    static void fft(CArray &x);
    static void DIF_FFT(CArray &x);
    static void ifft(CArray& x);


};

#endif // RAYMATH_H
