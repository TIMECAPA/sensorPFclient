#include "dndtoolbutton.h"

#include <QDrag>
#include <QMouseEvent>
#include <QApplication>
#include <QMimeData>
#include <QPainter>
#include <QPixmap>
#include <QBitmap>
#include <QAction>
#include <QDebug>

/**
 * @brief 생성자
 * @param title
 * @param parent
 */
DnDToolButton::DnDToolButton(const QString &title, QWidget *parent)
    : QToolButton(parent),
      m_bPressed(false),
      m_strTitle(title)

{

}

DnDToolButton::~DnDToolButton()
{

}

/**
 * @brief 마우스 Press 이벤트 처리
 * @param event
 */
void DnDToolButton::mousePressEvent(QMouseEvent *event)
{
    m_bPressed = true;

//    QAction* action = this->actionAt(event->pos());
//    if(action){
//        qDebug() << "mouseMoveEvent : " << action->text() << endl;
//    }
    setCursor(Qt::ClosedHandCursor);
    QToolButton::mousePressEvent(event);
}

/**
 * @brief 마우스 Move 이벤트 처리
 * @param event
 */
void DnDToolButton::mouseMoveEvent(QMouseEvent *event)
{

    //event->button() == Qt::LeftButton)

    if (QLineF(event->screenPos(), event->pos()).length() < QApplication::startDragDistance()) {
        return;
    }

    QDrag *drag = new QDrag(this);
    QMimeData *mime = new QMimeData;
    drag->setMimeData(mime);


    //if (n++ > 2 && (qrand() % 3) == 0) {
        QImage image(":/images/undo.png");
        mime->setImageData(image);
        mime->setText(m_strTitle);
        drag->setPixmap(QPixmap::fromImage(image).scaled(30, 40));
        drag->setHotSpot(QPoint(15, 30));

//    } else {
//        mime->setColorData(color);
//        mime->setText(QString("#%1%2%3")
//                      .arg(color.red(), 2, 16, QLatin1Char('0'))
//                      .arg(color.green(), 2, 16, QLatin1Char('0'))
//                      .arg(color.blue(), 2, 16, QLatin1Char('0')));

//        QPixmap pixmap(34, 34);
//        pixmap.fill(Qt::white);

//        QPainter painter(&pixmap);
//        painter.translate(15, 15);
//        painter.setRenderHint(QPainter::Antialiasing);
//        //paint(&painter, 0, 0);
//        painter.end();

//        pixmap.setMask(pixmap.createHeuristicMask());

//        drag->setPixmap(pixmap);
//        drag->setHotSpot(QPoint(15, 20));
//    }
    drag->exec();
    setCursor(Qt::OpenHandCursor);
    QToolButton::mouseMoveEvent(event);
}
/**
 * @brief 마우스 Release 이벤트 처리
 * @param event
 */
void DnDToolButton::mouseReleaseEvent(QMouseEvent *event)
{
    m_bPressed = false;
    setCursor(Qt::OpenHandCursor);
    QToolButton::mouseReleaseEvent(event);
}
