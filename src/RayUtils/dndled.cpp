#include "dndled.h"

#include <QtSvg>
#include <QSvgRenderer>

/**
 * @brief 생성자
 * @param parent
 */
DnDLED::DnDLED(QWidget *parent) : QWidget(parent)
{
    m_dbVal = 0.0;
    renderer = new QSvgRenderer();
}

/**
 * @brief 소멸자
 */
DnDLED::~DnDLED()
{
    delete renderer;
}

/**
 * @brief 컨트롤 페인트
 */
void DnDLED::paintEvent(QPaintEvent *)
{

    QString strLED=":/svg/circle_red.svg";
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    //ledShapeAndColor=shapes[m_shape];
    if(m_dbVal <= m_dbUpperLimit && m_dbVal >= m_dbLowerLimit){
        strLED = ":/svg/circle_green.svg";
    }else if(m_dbVal > m_dbUpperLimit){
        strLED = ":/svg/circle_red.svg";
    }else if(m_dbVal < m_dbLowerLimit){
        strLED = ":/svg/circle_yellow.svg";
    }else{
        strLED = ":/svg/circle_blue.svg";
    }

    renderer->load(strLED);
    renderer->render(&painter);
}

/**
 * @brief 컨트롤의 색 업데이트 처리
 */
void DnDLED::updateLEDColor()
{
    update();
}

/**
 * @brief 값 설정
 * @param dbVal
 */
void DnDLED::setValue(double dbVal)
{
    m_dbVal = dbVal;
    updateLEDColor();

}
/**
 * @brief 설정 값 가져오기
 * @return
 */
double DnDLED::value() const
{
    return m_dbVal;
}

/**
 * @brief 상한 값 설정
 * @param dbUpperLimit
 */
void DnDLED::setUpperLimit(double dbUpperLimit)
{
    m_dbUpperLimit = dbUpperLimit;
}

/**
 * @brief 상한 설정 값 가져오기
 * @return
 */
double DnDLED::upperLimit() const
{
    return m_dbUpperLimit;
}

/**
 * @brief 하한 값 설
 * @param dbLowerLimit
 */
void DnDLED::setLowerLimit(double dbLowerLimit)
{
    m_dbLowerLimit = dbLowerLimit;
}

/**
 * @brief 하한 설정 값 가져오기
 * @return
 */
double DnDLED::lowerLimit() const
{
    return m_dbLowerLimit;
}

/**
 * @brief 상한 값 사용 설정
 * @param bUsage
 */
void DnDLED::setUseUpperLimit(bool bUsage)
{
    m_bUseUpperLimit = bUsage;
}

/**
 * @brief 상한 값 사용 상태 가져 오기
 * @return
 */
bool DnDLED::useUpperLimit() const
{
    return m_bUseUpperLimit;
}

/**
 * @brief 하한 값 사용 설정
 * @param bUsage
 */
void DnDLED::setUseLowerLimit(bool bUsage)
{
    m_bUseLowerLimit = bUsage;
}

/**
 * @brief 하한 값 사용 상태 가져오기
 * @return
 */
bool DnDLED::useLowerLimit() const
{
    return m_bUseLowerLimit;
}
